﻿function HomeController($scope, $http, $uibModal) {
    var vm = this;

    // Assigned when map is loaded
    var bingMap = null;

    // Kep a track of pushpins
    var entryPushPinMap = new Map();

    function _invokeLatLngModal(latLngEntry) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'latLngEntryModal.html',
            controller: 'LatLngModalController as latLngModalVm',
            resolve: {
                currentLatLng: function () {
                    return latLngEntry;
                }
            }
        });

        return modalInstance.result;
    }

    function newEntry() {
        _invokeLatLngModal().then(function (latLng) {
            $http
                .post('/api/locations', latLng)
                .then(function (response) {
                    if (response.data) {
                        // We return the LatLng model created on the server.
                        var entry = response.data;

                        // Update UI
                        vm.entries.push(entry);

                        // Update Bing Map
                        var location = new Microsoft.Maps.Location(entry.lat, entry.lng);
                        var pushpin = new Microsoft.Maps.Pushpin(location, null);
                        bingMap.entities.push(pushpin);

                        // Associate entry ID to pushPin
                        entryPushPinMap.set(entry.ID, pushpin);

                        // Done
                        alert('New entry created!');
                    }
                })
                .catch(function () {
                    alert('Failed to create a new entry');
                });
        });
    }

    function updateEntry(entry) {
        _invokeLatLngModal(entry).then(function (updatedEntry) {
            $http
                .put('/api/locations/' + entry.ID, updatedEntry)
                .then(function (response) {
                    // Update orignal entry from remote source
                    entry.ID = updatedEntry.ID;
                    entry.lat = updatedEntry.lat;
                    entry.lng = updatedEntry.lng;

                    // Update bing map
                    var pushPin = entryPushPinMap.get(entry.ID);
                    if (pushPin !== undefined) {
                        pushPin.setLocation(new Microsoft.Maps.Location(entry.lat, entry.lng))
                    }

                    // Done
                    alert('Entry updated!');
                })
                .catch(function () {
                    alert('Failed to update entry');
                });
        });
    }

    function deleteEntry(entry) {
        $http.delete('/api/locations/' + entry.ID)
            .then(function () {
                var idx = vm.entries.indexOf(entry);
                if (idx !== -1) {
                    vm.entries.splice(idx, 1);
                }

                // Remove orignal from map
                var pushPin = entryPushPinMap.get(entry.ID);
                if (pushPin !== undefined) {
                    // Remove from bing map
                    bingMap.entities.remove(pushPin);

                    // Delete mapped record
                    entryPushPinMap.delete(entry.ID);
                }
            });
    }

    function fetchData() {
        $http.get('/api/locations').then(function (response) {
            vm.entries = response.data;

            // Set the markers on the map
            setMapEntries(vm.entries);
        });
    }

    function loadMap() {
        var element = $('#myMap')[0];
        bingMap = new Microsoft.Maps.Map(element, {
            credentials: 'Ao9gwCK3HcuIFXft5OEADr6s2s4425-nJzcV6MdZzLqAsFvRYTT4kCzmuyI-KvDu'
        });

        return bingMap;
    }

    function setMapEntries(entries) {
        for (var i = 0; i < entries.length; ++i) {
            var entry = entries[i];
            var loc = new Microsoft.Maps.Location(entry.lat, entry.lng);
            var pushpin = new Microsoft.Maps.Pushpin(loc, null);

            // Add PushPin to bing map
            bingMap.entities.push(pushpin);

            // Associate PushPin to entry.ID
            entryPushPinMap.set(entry.ID, pushpin);
        }
    }

    function zoomMap() {
        var locArray = [];
        for (var i = 0; i < vm.entries.length; ++i) {
            var entry = vm.entries[i];
            var loc = new Microsoft.Maps.Location(entry.lat, entry.lng);
            locArray.push(loc);
        }

        var locRect = new Microsoft.Maps.LocationRect.fromLocations(locArray);
        bingMap.setView({ bounds: locRect });
    }

    function init() {
        vm.newEntry = newEntry;
        vm.updateEntry = updateEntry;
        vm.deleteEntry = deleteEntry;
        vm.zoomMap = zoomMap;

        // Display the map
        loadMap();

        // Get the entries and set them to the map
        fetchData();
    }

    // vm.init = init;
    init();
}
HomeController.$inject = ['$scope', '$http', '$uibModal'];

function LatLngModalController($scope, $uibModalInstance, currentLatLng) {
    var vm = this;

    function cancel() {
        $uibModalInstance.dismiss();
    }

    function save() {
        $uibModalInstance.close(vm.latLng);
    }

    function init() {
        vm.cancel = cancel;
        vm.save = save;

        // make any current entry immutable as we dont want to mutate the
        // original if cancelled.
        vm.latLng = currentLatLng ? angular.copy(currentLatLng) : {};
    }

    init();
}

LatLngModalController.$inject = ['$scope', '$uibModalInstance', 'currentLatLng'];

angular
    .module('angular-mvc-app')
    .controller('HomeController', HomeController)
    .controller('LatLngModalController', LatLngModalController);