﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bingaling.Models
{
    public class LatLng
    {
        [Key]
        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 ID { get; set; }

        [Required]
        [Display(Name = "Latitude")]
        public Double lat { get; set; }

        [Required]
        [Display(Name = "Longitude")]
        public Double lng { get; set; }
    }
}