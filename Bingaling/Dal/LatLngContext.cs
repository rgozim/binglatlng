﻿using Bingaling.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Bingaling.Dal
{
    public class LatLngContext : DbContext
    {
        public LatLngContext() : base("LatLngContext")
        {
        }

        public DbSet<LatLng> Locations { get; set; }

    }
}