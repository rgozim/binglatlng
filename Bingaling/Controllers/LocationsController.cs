﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Bingaling.Dal;
using Bingaling.Models;
using System.Web.Mvc;

namespace Bingaling.Controllers
{
    public class LocationsController : ApiController
    {
        private LatLngContext db = new LatLngContext();

        // GET: api/Locations
        public IQueryable<LatLng> GetLocations()
        {
            return db.Locations;
        }

        // GET: api/Locations/5
        [ResponseType(typeof(LatLng))]
        public IHttpActionResult GetLatLng(int id)
        {
            LatLng latLng = db.Locations.Find(id);
            if (latLng == null)
            {
                return NotFound();
            }

            return Ok(latLng);
        }

        // PUT: api/Locations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLatLng(int id, LatLng latLng)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != latLng.ID)
            {
                return BadRequest();
            }

            db.Entry(latLng).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LatLngExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Locations
        [ResponseType(typeof(LatLng))]
        public IHttpActionResult PostLatLng(LatLng latLng)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Locations.Add(latLng);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = latLng.ID }, latLng);
        }

        // DELETE: api/Locations/5
        [ResponseType(typeof(LatLng))]
        public IHttpActionResult DeleteLatLng(int id)
        {
            LatLng latLng = db.Locations.Find(id);
            if (latLng == null)
            {
                return NotFound();
            }

            db.Locations.Remove(latLng);
            db.SaveChanges();

            return Ok(latLng);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LatLngExists(int id)
        {
            return db.Locations.Count(e => e.ID == id) > 0;
        }
    }
}